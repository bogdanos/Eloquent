<?php

namespace Eloquent\Service;
require_once "InjectionHandler.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class DropInjectionHandler extends InjectionHandler{

    public function __construct(){
        parent::__construct();
        $this->errorMessage = "DROP keyword found on query.";
    }

    public function check($query){
        
        if(preg_match("/(drop)/", $query)){
            $this->writeOnLogger();
            return 1;
        }
    }
}