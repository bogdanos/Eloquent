<?php
namespace Eloquent\Service\Logger;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


class TextLogger{

    private $logPath = __DIR__ . "/log.txt";

    public function write($message){
        
        date_default_timezone_set('Europe/Rome');
        $timedate = gmdate("Y-m-d G:i:s O");
        $ip = $_SERVER["HTTP_HOST"];
        error_log("ERROR: " . $message . "\nOn " . $timedate . "\nFrom IP: " . $ip . "\n\n", 
            3, $this->logPath);

    }
}