<?php
namespace Eloquent\Service;

require_once __DIR__ . "/logger/Logger.php";

use Eloquent\Service\Logger as L;
abstract class InjectionHandler{
    //logger
    private $logger = null;
    protected $errorMessage = "";

    public function __construct(){
        $this->logger = new L\TextLogger();
    }

    
    public abstract function check($query);

    protected function writeOnLogger(){
        if($this->logger) $this->logger->write($this->errorMessage);
    }
}