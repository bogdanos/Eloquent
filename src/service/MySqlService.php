<?php
namespace Eloquent\Service;

require_once 'constants/Constants.php';
require_once __DIR__ . "/DropInjectionHandler.php";

use Eloquent\Service as Constants;
use Eloquent\Service as H;
class MySqlService{

    private $handlers = array();

    /**
     * @var mysqli object
     */
    private $mysqlConnection = null;


    /**
     * @var array contains Database constants as host, username
     * and dbname
     */
    private $con = [];


  
    public function __construct($lazy = TRUE){
        $this->handlers["drop"] = new H\DropInjectionHandler();
        $this->con = Constants\Constants::$DB_SIMPLE_USER;
        if (!$lazy) $this->connect();
    }


    /**
     * Open connection with db;
     * @return void
     */
    private function connect(){
        $this->mysqlConnection = new \mysqli(
            $this->con['hostname'], 
            $this->con['username'], 
            $this->con['password'],
            $this->con['database']
        );
    }


    /**
     * @return queryResult if query succeeded
     */
    private function __execQuery(string $query){
        // $query = strtolower($query); // A mistake that took me 5 hour of my life.
        if($this->mysqlConnection){
            foreach($this->handlers as $k => $v){
                if($v->check($query)) return false;
            }
            $result = $this->mysqlConnection->query($query);
            return $result;
        }
        return FALSE;
    }
    
    
    /**
     * For insert type queries.
     * @return boolean if it succeeded or not
     */
    public function insert_query(string $query){
        if($this->mysqlConnection){
            $r = $this->__execQuery($query);
            return $r;
        }
    }

    
    /**
     * For SELECT type queries.
     * It takes the query and then fetches as
     * associative array all the result
     * @return mixed (is connected : result in an associative array ? FALSE)
     */
    public function select_query(string $query){

        if($this->mysqlConnection){
            $result = $this->__execQuery($query);
            if($result == FALSE) return FALSE;
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return FALSE;
    }


    public function close(){
        if($this->mysqlConnection)
            $this->mysqlConnection->close();
    }

}

?>