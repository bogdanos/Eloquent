<?php
namespace Eloquent\Service;

class Constants{
    public static $DB_SIMPLE_USER = [
        'hostname' => 'localhost',
        'username' => 'simple_user',
        'password' => 'simple_user_password',
        'database' => 'eloquent'
    ];

    public static $DB_ADMIN = [
        'username' => '',
        'password' => ''
    ];

    public static $DB_QUERIES = [
        'insert_user' => "INSERT INTO user(username, password) VALUES('%s', '%s');",
        'check_user' => "SELECT username, password FROM user WHERE username='%s';",
        
        'insert_new_post' => "INSERT INTO post(author, likes, content) values((select user.id from user where user.username = '%s'), 0, '%s');",  
        'home_posts' => "SELECT u.username as author, p.content, p.time, p.likes from user as u inner join post as p on p.author = u.id order by p.time DESC;",
    ];
}